#!/usr/bin/env python
"""Tests for `scidat` package."""
# pylint: disable=redefined-outer-name
from scidat import __version__
from scidat.scidat_interface import SciDatInterface
from scidat.scidat_impl import SciDat
import pandas as pd
from datetime import datetime

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_SciDatInterface():
    """ testing the formal interface (SciDatInterface)
    """
    assert issubclass(SciDat, SciDatInterface)

def test_SciDat():
    """ Testing SciDat class
    """
    sd = SciDat()
    assert isinstance(sd, SciDatInterface)
    sd.dataframe = pd.DataFrame({'a': [1, 2, 3], 'b': [4, 5, 6]})
    assert sd.dataframe.equals(pd.DataFrame({'a': [1, 2, 3], 'b': [4, 5, 6]}))

    sd.metadata = {'title': 'test', 'created': '2011-11-04T00:05:23Z'}
    assert sd.metadata.title == 'A Title' # should be fixed
     
    #assert sd.metadata.created == datetime.fromisoformat('2011-11-04T00:05:23Z')
    
