
# Acknowledgements and Credits

The SciDat project thanks


Contributors
------------

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* mark doerr <mark@uni-greifswald.de>