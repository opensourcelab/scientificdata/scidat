"""_____________________________________________________________________

:PROJECT: SciDat

* SciDat Main module implementation *

:details:  Main module implementation of SciDat.
              This module contains the implementation of the SciDat Interface with 
               convience methods for reading and writing.
                For metata storage, a SciDat object is used.
                The metadata is based on the [JSON-LD](https://json-ld.org/) standard
                and uses the Dublin Core Metadata Initiative (DCMI) terms - DCTERMS.

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


import logging
import json
from datetime import datetime
from pydantic import BaseModel, Field
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import fastparquet as fp

from rdflib import Graph

from rdflib import DC, FOAF, DCTERMS, XSD, Literal, Graph, Namespace, RDF, URIRef
from rdflib.namespace import DC, FOAF, DCTERMS, XSD, Namespace, RDF, URIRef, RDFS, SKOS, OWL

from scidat.scidat_interface import SciDatInterface

class SciDat(SciDatInterface):
    def __init__(self, 
                 dataframe=None,
                 metadata=None,
                 ):
        self.logger = logging.getLogger(__name__)
        self.logger.info("SciDat object created")

        if dataframe is not None:
            self._dataframe = dataframe
        else:
            self._dataframe = None

        if metadata is not None:
            self._metadata = metadata
        else:
            self._metadata = None
         
    # getter / setter for dataframe (in pandas with pyarrow backend and metadata added to the pyarrow table)
    @property
    def dataframe(self):
        """getter for dataframe """
        return self._dataframe
    
    @dataframe.setter
    def dataframe(self, dataframe: pd.DataFrame):
        """setter for dataframe """
        self._dataframe = dataframe

    # getter / setter for metadata (in JSON-LD)
    @property
    def metadata(self):
        """getter for metadata """
        return self._metadata
    
    @metadata.setter
    def metadata(self, metadata_dict : dict):
        """setter for metadata """
        self._metadata = SciDatMetadata( **metadata_dict)

    @property
    def metadata_jsonld(self):
        """getter for metadata as JSON-LD """
        return self._metadata.model_dump(by_alias=True)

    @property
    def metadata_rdf(self, format="turtle"):
        g = Graph()

        g.parse(data=self.metadata_jsonld, format="json-ld")

        return g.serialize(format=format)

    def write_parquet(self, parquet_filename: str, metadata_key: str = "scidat.org") -> None:
        """writing a parqet file
            add metadata to a pandas dataframe 

        :param parquet_filename: filename of the parquet file
        :type parquet_filename: str
        """
        self.logger.info("writing parquet file")

        table = pa.Table.from_pandas(self._dataframe)

        new_meta_json = self._metadata.model_dump_json(by_alias=True)
        existing_meta = table.schema.metadata
        combined_meta = {
            metadata_key.encode() : new_meta_json.encode(),
            **existing_meta
        }
        table = table.replace_schema_metadata(combined_meta)
        pq.write_table(table, parquet_filename, compression='GZIP')

    def read_parquet(self, parquet_filename: str,  metadata_key: str = "scidat.org") -> None:
        """reading a parqet file

        :param parquet_filename: filename of the parquet file
        :type parquet_filename: str
        """
        self.logger.info("reading parquet file")
        if parquet_filename is not None and parquet_filename.endswith(".parquet") or parquet_filename.endswith(".parq"):

            table = pa.parquet.read_table(parquet_filename)
            if self._dataframe is not None:
                raise Exception("Dataframe already set")
            self._dataframe = table.to_pandas()
            
            if self._metadata is not None:
                raise Exception("Metadata already set")
            restored_meta_json_raw = json.loads(table.schema.metadata[metadata_key.encode()])
            if restored_meta_json_raw is None:
                raise Exception("No metadata found in parquet file")
            self._metadata = SciDatMetadata(**restored_meta_json_raw)


class SciDatMetadata(BaseModel):
    """Main class, describing the JSON-LD based SciDat object
       The metadata is based on the [JSON-LD](https://json-ld.org/) standard
       and uses the Dublin Core Metadata Initiative (DCMI) terms - DCTERMS.
    """
    contexts : list = Field(default=[{"https://gitlab.com/opensourcelab/scientificdata/scidat/" : "scidat"}], 
                             alias="@context", description="Contexts of the SciDat object")
    ld_version : str = Field(default="1.0", alias="@version", description="JSON-LD version of the SciDat object")
    version_scidat : str = Field(default="0.1", description="Version of the SciDat object")

    ld_id : str = Field(default="https://gitlab.com/opensourcelab/scientificdata/scidat/", alias="@id", description="doc ID of the SciDat object")

   #  authors  = Field(default=[], description="Authors of the SciDat object", alias="author", title="Author", type="array", 
   #                  items={"type": "string"}, uniqueItems=True, minItems=1, maxItems=100, examples=["John Doe", "Jane Doe"], 
   #                  pattern="^[a-zA-Z0-9_ ]*$", minLength=1, maxLength=100, json_schema_extra={"iri": DCTERMS.creator  }  ) 
    description : str = Field(default="A description", alias="@description", description="Description of the SciDat object")
    title : str = Field(default="A Title", alias="@title", description="Title of the SciDat object")
    #type : str = Field(default="", alias="@type", description="Type of the SciDat object")

    # JSON-LD named graphs that contain a list of nested JSON-LD objects

    # https://json-ld.org/spec/latest/json-ld/#named-graphs
    ld_nameed_graphs : list = Field(default=[], description="Named graphs of the SciDat object", alias="@graph", title="Named graphs", type="array",
                                items={"type": "object"}, uniqueItems=True, minItems=1,) # maxItems=100, examples=["John Doe", "Jane Doe"],
                               # pattern="^[a-zA-Z0-9_ ]*$", minLength=1, maxLength=100, json_schema_extra={"iri": DCTERMS.creator  }  )
    
    # now follows a list of all common metadata fields from the DCMI

    # DCMI Metadata Terms
    # https://www.dublincore.org/specifications/dublin-core/dcmi-terms/

    authors : list[str] = Field(default=[], description="Authors of the SciDat object", alias="author", title="Author", type="array", 
                    items={"type": "string"}, uniqueItems=True, minItems=1 ) # maxItems=100, examples=["John Doe", "Jane Doe"], 
                   # pattern="^[a-zA-Z0-9_ ]*$", minLength=1, maxLength=100, json_schema_extra={"iri": DCTERMS.creator  }  )
    contributors : list[str] = Field(default=[], description="Contributors of the SciDat object", alias="contributor", title="Contributor", type="array",
                    items={"type": "string"}, uniqueItems=True, minItems=1 ) # maxItems=100, examples=["John Doe", "Jane Doe"],
    created : datetime = Field(default=datetime.now(), description="Creation date of the SciDat object", alias="created", title="Created", type="string",
                    format="date-time", examples=["2021-01-01T00:00:00Z"], json_schema_extra={"iri": DCTERMS.created  }  )
    # contributors = Field(default=[], description="Contributors of the SciDat object")
    # created = Field(default="", description="Creation date of the SciDat object")
    # creator = Field(default="", description="Creator of the SciDat object")
    # description = Field(default="", description="Description of the SciDat object")
    # format = Field(default="", description="Format of the SciDat object")
    # identifier = Field(default="", description="Identifier of the SciDat object")
    # language = Field(default="", description="Language of the SciDat object")
    # license = Field(default="", description="License of the SciDat object")
    # publisher = Field(default="", description="Publisher of the SciDat object")
    # relation = Field(default="", description="Relation of the SciDat object")
    # rights = Field(default="", description="Rights of the SciDat object")
    # source = Field(default="", description="Source of the SciDat object")
    # subject = Field(default="", description="Subject of the SciDat object")
    # title = Field(default="", description="Title of the SciDat object")
    # type = Field(default="", description="Type of the SciDat object")
    # # DCTERMS Metadata Terms
    # # https://www.dublincore.org/specifications/dublin-core/dcmi-terms/

    # abstract = Field(default="", description="Abstract of the SciDat object")
    # accessRights = Field(default="", description="Access rights of the SciDat object")
    # accrualMethod = Field(default="", description="Accrual method of the SciDat object")
    # accrualPeriodicity = Field(default="", description="Accrual periodicity of the SciDat object")
    # accrualPolicy = Field(default="", description="Accrual policy of the SciDat object")
    # alternative = Field(default="", description="Alternative of the SciDat object")
    # audience = Field(default="", description="Audience of the SciDat object")
    # available = Field(default="", description="Available of the SciDat object")
    # bibliographicCitation = Field(default="", description="Bibliographic citation of the SciDat object")
    # conformsTo = Field(default="", description="Conforms to of the SciDat object")
    # contributor = Field(default="", description="Contributor of the SciDat object")
    # coverage = Field(default="", description="Coverage of the SciDat object")
    # created = Field(default="", description="Created of the SciDat object")
    # creator = Field(default="", description="Creator of the SciDat object")
    # date = Field(default="", description="Date of the SciDat object")
    # dateAccepted = Field(default="", description="Date accepted of the SciDat object")
    # dateAvailable = Field(default="", description="Date available of the SciDat object")
    # dateCopyright = Field(default="", description="Date copyrigth")




class SciDataImpl(SciDatInterface):
    def __init__(self) -> None:
        """Implementation of the SciDatInterface
        """
        
    def greet_the_world(self, name: str) -> str:
        """greeting module - adds a name to a greeting

        :param name: person to greet
        :type name: str
        """
        logging.debug(f"Greeting: {name}")
        return f"Hello world, {name} !"        

